# dotfiles

## Required dependencies
- bat (optional)
- broot (optional)
- fm (optional)
- fzf (optional)
- micro (optional)
- nano (fallback, if micro isn't installed)
- paru
- [stow](https://www.gnu.org/software/stow/)
- zsh


## Install all dependencies in Arch Linux 

### Install paru

```bash
sudo pacman -S --needed base-devel
git clone https://aur.archlinux.org/paru.git
cd paru
makepkg -si
```

### Install the other dependencies
`paru -S --needed bat broot fm-bin fzf micro nano stow zsh`

### Set zsh as your default shell
`chsh -s /usr/bin/zsh`


## Using [GNU Stow](https://www.gnu.org/software/stow/) to manage dotfiles

Inspired by https://www.stevenrbaker.com/tech/managing-dotfiles-with-gnu-stow.html

### What is Stow

GNU Stow is a symlink farm manager which takes distinct packages of software and/or data located in separate directories on the filesystem, and makes them appear to be installed in the same place. For example, /usr/local/bin could contain symlinks to files within /usr/local/stow/emacs/bin, /usr/local/stow/perl/bin etc., and likewise recursively for any other subdirectories such as .../share, .../man, and so on.
This is particularly useful for keeping track of system-wide and per-user installations of software built from source, but can also facilitate a more controlled approach to management of configuration files in the user's home directory, especially when coupled with version control systems. 


### Workflow

If you keep your dotfiles in a VCS repository, setting up a new machine is as easy as:

`git clone https://codeberg.org/sam01/dotfiles.git && cd dotfiles && stow */`    

or

`git clone git@codeberg.org:sam01/dotfiles.git && cd dotfiles && stow */`



Now, for each program you have dotfiles for, move them into a directory inside your dotfiles working copy. For example, if you want to stow your `git` and `bash` dotfiles, you might do the following:

~~~bash
mkdir git
mv .git* dotfiles/git
mv .bash_profile .bashrc .bash_aliases dotfiles/bash
cd dotfiles && stow */
~~~

Whenever you make changes to your dotfiles _in place_, you will need to remember to commit those changes. If you add new files, you will have to remember to re-run stow: `cd ~/dotfiles && stow */`. If you’ve removed files since the last run, you should re-stow: `cd ~/dotfiles && stow -R */`.

Finally, if you decide that you would like to un-stow all of your dotfiles for whatever reason, you can `cd ~/dotfiles && stow -D`.

